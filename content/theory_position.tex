\subsection{Position}
\label{sub:position}
The position of the object in a frame can be described relative to the \emph{field of view} of the camera ($\phi_x$, $\phi_y$).
A simple, linear approximation for the angle of the object compared to the centre of the image can be taken as follows,
where $\alpha$ denotes the horizontal angle and $\beta$ the vertical angle:

\begin{align}
  \alpha &= \left(\frac{x}{x_\text{max}} - \tfrac12 \right) \phi_x \\
  \beta  &= -\left(\frac{y}{y_\text{max}} - \tfrac12 \right) \phi_y
\end{align}

In another linear approximation, these angles are added to the pan ($\gamma$) and tilt ($\epsilon$) angles of the camera to produce the pan ($\psi$) and tilt angle ($\theta$) of the object:

\begin{align}
  \psi   &= \alpha + \gamma \\
  \theta &= \beta  + \epsilon
\end{align}

The following relations between the cameras apply,
where $h$ is horizontal distance from the camera to the object and $l$ the distance between the cameras:

\begin{align}
  h_1 &= l \cdot \frac
        { \sin(\SI{90}{\degree} - \psi_2)}
        { \sin(\psi_1 - \psi_2)}
       = l \cdot \cos\psi_2 \cdot \csc(\psi_1 - \psi_2) \\
  h_2 &= l \cdot \frac
        { \sin(\SI{90}{\degree} - \psi_1)}
        { \sin(\psi_1 - \psi_2)}
       = l \cdot \cos\psi_1 \cdot \csc(\psi_1 - \psi_2)
\end{align}

The object's location ($(x,y,z)$) in a three dimensional reference frame with the origin at camera one and camera two positioned at $(l,0,0)$ can then be determined:

\begin{align}
  x &= h_1 \cdot \sin\psi_1   = l - h_2 \cdot \sin\psi_2 \\
  y &= h_1 \cdot \cos\psi_1   = h_2 \cdot \cos\psi_2 \\
  z &= h_1 \cdot \tan\theta_1 = h_2 \cdot \tan\theta_2
\end{align}

\subsection{Ballistic modelling}
The behaviour of an object on a ballistic trajectory can be modelled as follows, where the positions $x(t)$ and $y(t)$, and their change is known:

\begin{align}
  v_x(t)   &= \pd{x(t)}{t} \\
  v_y(t)   &= \pd{y(t)}{t} \\
  v_x(t+\tau) &= v_x(t) - \tau \cdot h \\
  v_x(t+\tau) &= v_y(t) - \tau \cdot g \\
  x(t+\tau)   &= x(t) + \tau \cdot v_x(t+n) \\
              &= x(t) + \tau \cdot \pd{x(t)}{t} - \tau^2 h \\
  y(t+\tau)   &= y(t) + \tau \cdot v_y(t+n) \\
              &= y(t) + \tau \cdot \pd{y(t)}{t} - \tau^2 g
\end{align}

Where $h$ is the horizontal deceleration due to air resistance and $g$ is the acceleration due to gravity.

Actual acceleration can also be directly calculated from samples:

\begin{align}
  \Delta t_n &= t_n - t_{n-1} \\
  v_n        &= \frac{x_n - x_{n-1}}{\Delta t_n} \\
  a_n        &= 2 \frac{v_n - v_{n-1}}{\Delta t_n + \Delta t_{n-1}}
\end{align}
