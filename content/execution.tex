\section{Execution}

\subsection{FPGA component simulation}

\begin{figure*}[ht]
  \includegraphics{graphics/quad_sim}
  \margincaption{Part of the simulation result for the quadrature decoder%
    \label{fig:sim:quad}}
\end{figure*}

\begin{figure*}[ht]
  \includegraphics{graphics/pwm_sim}
  \margincaption{Part of the simulation result for the PWM generator%
    \label{fig:sim:pwm}}
\end{figure*}

Both the quadrature decoder and the PWM generator are simulated before deployment on the FPGA\@.
The simulation is used to verify the functionality of these components before attaching real hardware to the modules.
Faults in the design can be easily detected while no harm is possible to real world components.

The functionality of the quadrature decoder is tested by a \emph{fuzzing} test bench.
The test bench generates a configurable amount of cycles.
For each cycle a random direction, including no direction, a speed and a number of steps is generated.
This is then applied to the quadrature decoder and the counter output of the quadrature decoder is verified with the expected value.
A small excerpt of the generated waveform is visible in figure~\ref{fig:sim:quad}.
A change in direction and speed is visible.
As shown, the counter value (\lstinline{quad}) responds to every change in state.

For the PWM generator, a more classical test bench is used.
Here the result is verified visually with the graphs.
This is done because it is too difficult to convert the generated PWM signal back to a verifyable value.
A small excerpt of the generated data is visible in figure~\ref{fig:sim:pwm}.
Visible is the generated PWM signal (\lstinline{out1}) and the direction signals (\lstinline{ha1} and \lstinline{hb1}).
The maximum counter value (\lstinline{top}) is configured at 400 for this test.
Halfway through the test, the compare value (\lstinline{st_data_out}) changes from 250 to -30.
Visible is that the change in PWM duty cycle and the polarity of the direction signals remain at their old values until a new cycle starts.


\subsection{Controller tuning}
\begin{margintable}
  \centering
  \begin{tabular}{l r r}
    \toprule
    Param. & Pan & Tilt \\
    \midrule
    $K_p$ & 13.0   & 3.2 \\
    $C_i$ & 0.0    & 0.0 \\
    $C_d$ & 3000.0 & 3000.0 \\
    \bottomrule
  \end{tabular}
  \caption{Found PID controller parameters.%
    \label{tab:exec:pid:params}}
\end{margintable}

\begin{marginfigure}
  \hspace{-.1\linewidth}%
  \resizebox{1.1\linewidth}{!}{\input{graphics/netstep_pan}}
  \vspace{-2em}
  \caption{Response of the tuned pan controller.%
    \label{fig:netstep:pan}}
\end{marginfigure}

\begin{marginfigure}
  \hspace{-.1\linewidth}%
  \resizebox{1.1\linewidth}{!}{\input{graphics/netstep_tilt}}
  \vspace{-2em}
  \caption{Response of the tuned tilt controller.%
    \label{fig:netstep:tilt}}
\end{marginfigure}

With the trial-and-error tuning method, starting from the controller parameters found in section~\ref{sub:met:conv},
controller values for the pan and tilt PID controllers are found (table~\ref{tab:exec:pid:params}).
$C_i$ is set to 0 because the cameras have to track a highly dynamic object.
This results in realistic scenarios having no steady state error,
but the \emph{I} component of the controller only causing overshoot.
This overshoot may cause the camera to lose the object.

The step responses of the pan and tilt controllers are shown in figure~\ref{fig:netstep:pan} and~\ref{fig:netstep:tilt} respectively.
The speed of the response is mainly limited by the mechanical components, which can be seen from the fact that the response is mostly linear.
The tilt controller also shows a steady state error caused by the lack of \emph{I} component in the controller.

\subsection{Detection and tracking}
\begin{marginfigure}
  \centering
  \includegraphics{execution_snap}
  \caption{The JIWY tracking a fast-moving yellow ball.%
    \label{fig:exec:snap}}
\end{marginfigure}

The detection of a yellow ball and subsequent tracking works well with the calculated controller parameters.
Tests show that the camera is able to keep up even with a relatively fast-moving or thrown ball.
Figure~\ref{fig:exec:snap} shows the camera tracking a fast-moving yellow ball that could not be caught by one of the authors.
The tracking only works successfully when the distance between the ball and the camera is between 15 and 200 cm.
A recorded video of the operation (\texttt{demovideo.mp4}) is included with this report.

For detection, advanced filtering and detection was attempted.
This resulted in a performance of 4 frames per second.
The simple detection scheme as described in section~\ref{sub:vision}
was able to process frames at the 30 frames per second required,
with a CPU usage of 45\%.
This allows ample margin for running the detection with two cameras.

\subsection{Dual detection and physics}
The detection of a yellow ball with two cameras is tested.
The resulting position that is accurate within the measurement error (5 cm).
The landing prediction was tested by dropping or throwing the ball.
This yielded a landing point that was accurate within 15 cm.
Because of the low heights, time until landing was not accurately assessed.
