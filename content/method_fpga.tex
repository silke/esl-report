\subsection{FPGA}

The FPGA is used to implement the PID control loop.
By using the FPGA for the control loop, real time loop timings can be guaranteed.
The control loop consists of three parts:

\begin{itemize}
	\item The \emph{quadrature decoder}: The signals from the quadrature encoder needs to be converted to a single number indicating the angle of the axis.
	\item The \emph{PWM generator}: Each motor is controlled via one PWM signal and two directional signals.
	\item The \emph{PID controlled}: The PID loop controls the PWM generator based on the feedback from the quadrature decoder and the setpoint.
\end{itemize}

The whole control loop is implemented in a streaming data fashion based on the Avalon-ST bus.
The pipelined nature of the quadrature decoder, PID-control and PWM controller is suitable for the streaming design of this bus.
The PID pipeline is visible in figure~\ref{fig:schem:pid}.
This shows a single PID pipeline as implemented on the FPGA\@.
The full FPGA implementation as described here is visible in figure~\ref{fig:schem:full}.
This schematic shows the GPMC communication module together with four PID control loop instances.

All components are designed such that configuration parameters can be written at runtime via a memory mapped bus (either GPMC or the Avalon MM bus).
By using runtime configurations, the components can be used for both the pan and the tilt controller and idividually tuned to the physical setup.

\subsubsection{Quadrature decoder}

The implementation of the quadrature decoder is based on a state machine.
The current inputs are compared to the previous inputs.
Depending on the phase change between these values, the counter is either incremented or decremented.
This results in every input change counted as a counter change, instead of increments per full cycle, increasing the accuracy of the encoder.

A combination of a streaming data bus and memory mapped interface is used to communicate with other modules.
The streaming bus is used to provide the new encoder values within the loop deadline.
The memory mapped interface provides read and write access to the encoder value.

To prevent measurement artifacts due to random changes on the inputs,
a debounce circuit is placed before the quadrature decoder.
This circuit samples the inputs from the quadrature encoders at \SI{50}{kHz}.
Eight samples are stored and compared with each other.
The debounce circuit delays the inputs from the quadrature decoder until all measured samples are equal.

\subsubsection{PWM generator}

The design of the PWM generator is based on a counter and compare value.
Every clock tick, the counter is incremented. If the counter is below the compare value, the PWM output is 1, otherwise it is 0.
The Pulse Width Modulator is designed to be configurable, so that the maximum value of the counter is configurable.
Configuration values are only updated when the maximum value is reached.
This prevents glitches in the output of the PWM generator that could happen if the compare value is update halfway through a cycle.
Direction is determined by the sign of the duty cycle.

The frequency of the PWM generator depends on the configured maximum value.
This, in effect, provides a divider value from the \SI{50}{MHz} clock of the FPGA.

The duty cycle of the PWM modulator is provided over the streaming bus.
A new value is requested at a configurable delay before a new cycle starts.
This delay value can be tuned to achieve minimal delay between the measurement of the quadrature decoder and the update of the new value.

\subsubsection{PID controller}

The PID controller is designed to respond to data requests from the PWM generator.
Each time a value is requested by the PWM generator, the PID controller requests a new measurement from the quadrature decoder and uses this value to determine the next output.

Other settings such as the setpoint and PID settings are provided over the memory mapped interface.
By keeping all settings dynamic, the PID loop can be runtime adjusted to new settings.

Note that the PID controller does not take the sample time into account.
This results in the following equation for the output:

\[ u(t) = K_p e(t) + C_i \sum_{i=1}^k e(t) + C_d e(t) \]

Where $K_p$, $C_i$ and $C_d$ are all 32 bit fixed point numbers with 16 bits before and after the point.
How this affects the calculation and tuning of the parameters is described in section~\ref{sub:met:conv}.

The output (PWM setpoint) of the PID controller can be limited to never exceed a configurable limit.
