\subsection{Design Space Exploration}
\label{sub:dse}
For each block in the design, a choice is made between three platforms:

\begin{itemize}
  \item \emph{CPU}:  the Linux platform running on the Gumstix.
  \item \emph{NIOS}: a flashable extensible soft-core IP block on an FPGA.
  \item \emph{FPGA}: a completely synthesised implementation on an FPGA.
\end{itemize}

The choice of platform is make based on the following criteria:

\begin{itemize}
  \item \emph{Performance}:   how fast can the operation be performed.
  \item \emph{Flexibility}:   how easy it is to modify or tune the design.
  \item \emph{Communication}: how easy it is to communicate with required hardware.
  \item \emph{Development}:   how fast can an implementation be developed.
  \item \emph{Score}:         weighted average of the above.
                              Criteria that are deemed more important weigh more and vice versa.
\end{itemize}

Other criteria where considered but discarded:

\begin{itemize}
  \item \emph{Accuracy}:       all building blocks can be implemented with the same accuracy in calculations.
  \item \emph{Area/Footprint}: The FPGAs on both the RaMstix and the DE0 NANO should have sufficient space to allow all blocks to be implemented on them.
\end{itemize}

\subsubsection{Object detection}

\begin{table*}[ht]
  \begin{tabular}{l c c c c c}
    \toprule
    Platform & Performance & Flexibility & Communication & Development & Score \\
    \midrule
    CPU  &  –  & + + & + + & + + & + + \\
    NIOS & – – & +/– & – – &  –  &  –  \\
    FPGA & + + & – – & – – & – – & – – \\
    \bottomrule
  \end{tabular}
  \margincaption{Trade-off table for object detection on various platforms%
    \label{tab:dse:object-detection}}[8mm]
\end{table*}

The requirements for detecting objects are:

\begin{itemize}
  \item Performance of at least 30 Hz.
  \item Flexibility in which object to detect.
\end{itemize}

While the performance of the CPU is not as good, the platform should easily reach 30 Hz.
The flexibility and development time of this platform receive a very good grade because a library for object detection (OpenCV) can be used.
Communication with the webcam is also easy because of available USB and camera drivers.

Getting any kind of object detection library to work on the NIOS core is hard,
as minimal object detection libraries are hard to port and hard to use.
This results in less flexible software that takes significantly more time to develop.
Communication with the webcam is hard because of lack of NIOS USB hardware IP and the required webcam drivers.

On the FPGA the expected performance is very high.
Because everything is specifically implemented in dedicated hardware, however,
the platform is not flexible at all and development takes a significant amount of time.
Communication with the webcam is hard because of the need of synthesizable USB drivers.

Because the required performance is not very high, this criteria is left out of the score.
The rest of the valuations clearly show that the \emph{CPU} is the best platform for implementation.

\subsubsection{3D/Physics calculation}

\begin{table*}[ht]
  \begin{tabular}{l c c c c c}
    \toprule
    Platform & Performance & Flexibility & Communication & Development & Score \\
    \midrule
    CPU      &  +  & + + & n/a & + + & + + \\
    NIOS     & +/– &  +  & n/a &  +  &  +  \\
    FPGA     & + + & +/– & n/a & +/– & +/– \\
    \bottomrule
  \end{tabular}
  \margincaption{Trade-off table for 3D/physics calculation on various platforms%
    \label{tab:dse:physics-calculation}}[8mm]
\end{table*}

The main requirement for the 3D and physics calculations is that is has to perform at at least 30 Hz.
Because the input data is produced by the object detection, the communication has no requirements.

On the CPU the performance is satisfactory, and the algorithms are easy to implement and tune.

The same code can run on the NIOS resulting in a similar evaluation.
The performance is not as good as can be expected on the CPU (but only slightly so).
The flexibility and development, however, suffer due to having to add and program the NIOS core itself.

The best performance can be expected on the FPGA\@.
Development is harder because the implementation requires several sine functions.
The platform is also less flexible because of the hardware-focussed design.

Because the required performance is not very high, this criteria is left out of the score.
The rest of the valuations clearly show that the \emph{CPU} is the best platform for implementation.

\subsubsection{Location conversion}

\begin{table*}[ht]
  \begin{tabular}{l c c c c c}
    \toprule
    Platform & Performance & Flexibility & Communication & Development & Score \\
    \midrule
    CPU      & + + & + + & + + & + + & + + \\
    NIOS     & + + &  +  & + + & + + & + + \\
    FPGA     & + + &  +  &  +  &  +  &  +  \\
    \bottomrule
  \end{tabular}
  \margincaption{Trade-off table for location conversion on various platforms%
    \label{tab:dse:location-conversion}}[8mm]
\end{table*}

Location conversion (the conversion of angles to encoder values and vice versa)
has no major requirements and can be expected to perform the same on each platform.
The implementation on the CPU is more flexible because it can be integrated in the blocks where conversion is required.
Development and communication on the FPGA is harder because of the required conversions between floating and integer numbers.

Because of the lack of large differences these conversion blocks will be implemented on the platform where floating point numbers are required.
This will either be the \emph{CPU} or the \emph{NIOS} depending on the results of other evaluations.

\subsubsection{PID controller}

\begin{table*}[ht]
  \begin{tabular}{l c c c c c}
    \toprule
    Platform & Performance & Flexibility & Communication & Development & Score \\
    \midrule
    CPU      &  –  & + + & +/- & + + &  –  \\
    NIOS     & +/– & + + & + + &  +  &  +  \\
    FPGA     & + + & + + & + + &  +  & + + \\
    \bottomrule
  \end{tabular}
  \margincaption{Trade-off table for the PID controller on various platforms%
    \label{tab:dse:pid}}[8mm]
\end{table*}

The requirements for the PID controller are:

\begin{itemize}
  \item Guaranteed performance matching the rest of the control loop (>\SI{1}{kHz}).
  \item Flexibility in tuning parameters.
\end{itemize}

The real-time requirements of the PID controller can not be guaranteed on the CPU, and might therefore not match the performance of the rest of the control loop.
An implementation, however, can be very flexibly tuned to reduce the impact of the non real-time scheduler.
Development is also very straight forward, the required floating point operations are very suitable for the CPU platform.
Communication should be implemented over a chosen communication bus, which also results in a lack of guarantees.

The expected performance on the NIOS can be guaranteed, but is not very fast.
The flexibility is approximately the same as for the CPU\@.
When communication is between NIOS/NIOS or NIOS/FPGA the communication performance can also be guaranteed.
The development is similar to that of the CPU, but has the added hurdle of requiring a NIOS core.

The performance on the FPGA can be guaranteed and optimised to match the rest of the control loop.
The tuning can be implemented via the communication, allowing a fully tunable design.
The development is not as easy due to having to implement the PID algorithms in VHDL.

Because the performance is the most important requirement and the flexibility is the same for all platforms
the performance is given the most weight.
The resulting scores show that the PID controller can best be implemented on the \emph{FPGA}.

\subsubsection{PWM generator}

\begin{table*}[ht]
  \begin{tabular}{l c c c c c}
    \toprule
    Platform & Performance & Flexibility & Communication & Development & Score \\
    \midrule
    CPU      &  +  & + + &  +  &  +  &  +  \\
    NIOS     & + + & + + & + + &  +  &  +  \\
    FPGA     & + + & + + & + + & + + & + + \\
    \bottomrule
  \end{tabular}
  \margincaption{Trade-off table for the PWM on various platforms%
    \label{tab:dse:pwm}}[8mm]
\end{table*}

The main requirement for the PWM is performance
because it should be able to drive the camera motors at the desired speed.

While the CPU has on-board PWM,
the performance of \emph{changing} the parameters cannot be guaranteed.
Communication between the on-board PWM and the motors also requires changing some wiring and possibly soldering.
This also makes development harder and longer.

The NIOS core has guaranteed performance and has easy communication to the motor drivers.
Development is a bit harder, however, as it requires a NIOS core and the required driver code.

The FPGA implementation also offers hard performance guarantees.
Communication is easy because the mapping of signals is known.
Development is also easy because it has been completed in the assignments.

Because the main differences are in development and performance, the score is weighted towards these two.
The resulting score shows that the PWM can best be implemented on the \emph{FPGA}.

\subsubsection{Quadrature Decoder}

\begin{table*}[ht]
  \begin{tabular}{l c c c c c}
    \toprule
    Platform & Performance & Flexibility & Communication & Development & Score \\
    \midrule
    CPU      &  +  & + + &  +  & +/– & +/– \\
    NIOS     & + + & + + & + + &  +  &  +  \\
    FPGA     & + + & + + & + + & + + & + + \\
    \bottomrule
  \end{tabular}
  \margincaption{Trade-off table for the quadrature decoder on various platforms%
    \label{tab:dse:qe}}[8mm]
\end{table*}

The main requirement for the quadrature decoder is performance
because it provides the feedback in the control loop.
The main performance aspect in this situation is the real-time guarantees of the measurements.

While the encoding is relatively simple.
The CPU cannot guarantee performance either in the required operation or the communication.
Communication between the GPIO of the CPU and the required signals may also require changing some wiring.
This also makes development harder.

The NIOS core has guaranteed performance and can read the required signals without changing the wiring.
Development is a bit harder, however, as it requires a NIOS core and driver code.

The FPGA implementation also offers hard performance guarantees.
Communication is easy because the mapping of signals is known.
Development is also easy because it has been completed in the assignments.

Because the main differences are in development and performance, the score is weighted towards these two.
The resulting score shows that the quadrature decoder can best be implemented on the \emph{FPGA}.

\subsubsection{Communication}

\begin{table*}[ht]
  \begin{tabular}{l c c c c c}
    \toprule
    Platform & Performance & Simplicity & Development & Score \\
    \midrule
    GPMC & + + & + + & + + & + + \\
    UART &  –  &  –  & +/– & +/– \\
    SPI  &  +  &  –  & +/– &  + \\
    \bottomrule
  \end{tabular}
  \margincaption{Trade-off table for the available communication platforms%
    \label{tab:dse:communication}}[8mm]
\end{table*}

There are three ways of communicating between CPU and NIOS/FPGA:
GPMC (on the RaMstix), UART and SPI (both on the DE0-Nano).
These are evaluated on performance, simplicity, and development time.

Performance is a combination of low latency and high throughput.
Both are important: the first ensures attainment of the real-time targets,
while the second allows more data to be sent within the deadline (which can be used in various ways).

Other criteria, such as flexibility, are not a decision priority as interfacing on both sides does not greatly differ per communication platform.
An additional criteria, \emph{simplicity} (the inverse of \emph{complexity}),
reflects the amount of pre- and post-processing that needs to be performed before and after transfer of the data and the overhead caused thereby.

GPMC provides memory-mapped communication between the Overo Firestorm-P and the Cyclone III on the RaMstix.
Being a memory-mapped interface with split data and address lines, it is relatively fast (up to \SI{20}{MB/s}) with a low latency,
and allows specific data to be written to a pre-determined location without complex post processing.
This results in a high simplicity score.
Development time is short because a driver and implementation example are available.

UART is an asynchronous serial communication mechanism.
On the FPGA and CPU platform, the implementation (including the hardware wiring) has to be created entirely from scratch.
The addressing and specifying what data should go to what component has te be implemented on all platforms.
This results in a low simplicity score.
The throughput of UART is configurable, but usually limited to slow speeds (up to 115200 baud, but affected by lack of simplicity),
but with a low latency.

SPI is a synchronous serial communication protocol.
On the FPGA and CPU platform, the implementation (including the hardware wiring) has to be created entirely from scratch,
The addressing and specifying what data should go to what component has te be implemented on all platforms.
This results in a low simplicity score.
The performance depends on the clock speed of the sender/receiver and can be reasonably high (up to \SI{10}{MB/s}, but affected by lack of simplicity) with a low latency.

\subsubsection{Component decisions}

\begin{table}[ht]
  \centering
  \begin{tabular}{l c c c c c}
    \toprule
    Platform & Slow (total score) & Fast (total score) \\
    \midrule
    CPU  & + + + + + + & +/-         \\
    NIOS & + +         & + + +       \\
    FPGA & -           & + + + + + + \\
    \bottomrule
  \end{tabular}
  \caption{Trade-off table for the platforms per part}%
  \label{tab:dse:sum}
\end{table}

As described in section~\ref{sub:model},
the design can be split up in two parts: \emph{slow} and \emph{fast}.
The component score can be summarized per part, as shown in table~\ref{tab:dse:sum}.
The results are clear:

\begin{itemize}
  \item The slow part of the design is implemented on the CPU platform,
    this consists of the \emph{object detection}, \emph{3D/physics calculations} and the \emph{angle/location conversions}.
  \item The fast part of the design is implemented on the FPGA platform,
    this consists of the \emph{PID controller}, \emph{PWM generator} and the \emph{quadrature decoder}.
  \item The two parts communicate via \emph{GPMC} on the \emph{RaMstix}.
\end{itemize}
